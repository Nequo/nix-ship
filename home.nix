{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "nequo";
  home.homeDirectory = "/home/nequo";
  home.packages = with pkgs; [
    nix-direnv
  ];

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  programs.bash = {
    enable = true;
    initExtra = builtins.readFile ./bashrc;
    shellAliases = {
      "direnv" = "/nix/store/3vql64vwk85jbqa14vw66nd2ngsqgv03-direnv-2.32.1/bin/direnv";
    };
  };
  
  programs.git = {
    enable = true;
    userName = "Nadim Edde Gomez";
    userEmail = "nadim@eddegomez.org";
    aliases = {
      prettylog = "...";
    };
    extraConfig = {
      core = {
        editor = "vim";
      };
      color = {
        ui = true;
      };
      push = {
        default = "simple";
      };
      pull = {
        ff = "only";
      };
      init = {
        defaultBranch = "main";
      };
    };
    ignores = [
      ".DS_Store"
      "*.pyc"
    ];
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";
}