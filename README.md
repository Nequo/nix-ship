# Nix-ship

This repository holds my Nix configurations.
These are the machines that I currently use this repository for:

- Gold: Gaming laptop with Windows 11. Nix is installed over opensuse tumbleweed in WSL2.

## Command list

Install nix:
```bash
curl -L https://nixos.org/nix/install | sh
source $HOME/.nix-profile/etc/profile.d/nix.sh
```

Enable the use of [Flakes](https://nixos.wiki/wiki/Flakes).
```bash
mkdir -p ~/.config/nix
echo 'experimental-features = nix-command flakes' >> ~/.config/nix/nix.conf
```

Build and activate the home-manager flake:
```bash
nix build --no-link path:.#homeConfigurations.nequo.activationPackage
"$(nix path-info .#homeConfigurations.nequo.activationPackage)"/activate
```